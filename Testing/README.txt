'Testing' contains all files relevant to testing our Property Tycoon.
It has two parent directories, whose files are organised into sub-folders named after the current build version of the game.
    -- 'Change Lists', which stores documents listing changes to be made, as discovered
        in testing.
    -- 'Log Files', which stores logs of errors in the code. Peter will use these to fix specific errors.
        If an error window appears in the bottom right corner of the game during testing, 
        press the log file button, save it locally to your computer and then push it to SourceTree
        using the naming convention demonstrated below.

        	output_log_Toby_12-34pm_25.04.2018_build_1.0
        
        Please adhere to this naming convention for consistency.
        ALWAYS include the TIME AND DATE, plus the correct build number so log files and
        change lists are never mixed up.

        Lastly, I've also provided a simple template for documenting any changes to make.

Many thanks,

Toby

Last modified: 25/04/2018
