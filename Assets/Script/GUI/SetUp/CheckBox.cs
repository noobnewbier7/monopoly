﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CheckBox : Button
{
    const string EXPECTED_CHILD_NAME = "Check";
    Image _checkImage;

    protected override void Awake()
    {
        //throw exception if there are no check image as a child
        _checkImage = transform.Find(EXPECTED_CHILD_NAME).GetComponent<Image>();
        _checkImage.gameObject.SetActive(false);
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        _checkImage.gameObject.SetActive(!_checkImage.gameObject.activeInHierarchy);
    }
}
