﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSettingGUI : MonoBehaviour
{

    // Monobehaviour should not be instantiate by constructor, thus not feasible to set it as readonly
    static Dictionary<ModelType, Sprite> typeAndIcons;
    static Dictionary<ModelType, Sprite> typeAndCard;
    static Dictionary<ModelType, GameObject> typeAndModels;

    private PlayerType _playerType;
    private ModelType _modelType;
    private List<Sprite> _icons;

    [SerializeField] Image _icon;
    [SerializeField] AIDifficultySettingGUI _aIDifficultySetting;
    [SerializeField] PlayerTypeButton _playerTypeButton;

    private void Awake()
    {
        //set icons if it is not set yet
        if (typeAndIcons == null)
        {
            //sprites
            typeAndIcons = new Dictionary<ModelType, Sprite>();
            typeAndIcons[ModelType.WorkBoot] = Resources.Load<Sprite>("Sprite/UsableGameLogos/PlayingPieces/Boot");
            typeAndIcons[ModelType.Cat] = Resources.Load<Sprite>("Sprite/UsableGameLogos/PlayingPieces/Cat");
            typeAndIcons[ModelType.Goblet] = Resources.Load<Sprite>("Sprite/UsableGameLogos/PlayingPieces/GobletOfWine");
            typeAndIcons[ModelType.TopHat] = Resources.Load<Sprite>("Sprite/UsableGameLogos/PlayingPieces/HatStand");
            typeAndIcons[ModelType.Phone] = Resources.Load<Sprite>("Sprite/UsableGameLogos/PlayingPieces/Smartphone");
            typeAndIcons[ModelType.Spoon] = Resources.Load<Sprite>("Sprite/UsableGameLogos/PlayingPieces/Spoon");

            //3D Models
            typeAndModels = new Dictionary<ModelType, GameObject>();
            typeAndModels[ModelType.WorkBoot] = Resources.Load<GameObject>("Prefab/GamePlay/WorkBoot");
            typeAndModels[ModelType.Cat] = Resources.Load<GameObject>("Prefab/GamePlay/Cat");
            typeAndModels[ModelType.Goblet] = Resources.Load<GameObject>("Prefab/GamePlay/Goblet");
            typeAndModels[ModelType.TopHat] = Resources.Load<GameObject>("Prefab/GamePlay/TopHat");
            typeAndModels[ModelType.Phone] = Resources.Load<GameObject>("Prefab/GamePlay/Phone");
            typeAndModels[ModelType.Spoon] = Resources.Load<GameObject>("Prefab/GamePlay/Spoon");
        }

        _icons = typeAndIcons.Values.ToList();

    }

    public void NextIcon()
    {
        int currentIndex = _icons.FindIndex(sprite => sprite == _icon.sprite);
        currentIndex = currentIndex == typeAndIcons.Count - 1 ? 0 : currentIndex + 1; //reset if needed
        _icon.sprite = _icons[currentIndex];
    }

    public void LastIcon()
    {
        int currentIndex = _icons.FindIndex(sprite => sprite == _icon.sprite);
        currentIndex = currentIndex == 0 ? typeAndIcons.Count - 1 : currentIndex - 1; //reset if needed
        _icon.sprite = _icons[currentIndex];
    }

    public void SetPlayerType(PlayerType type)
    {
        _playerType = type;
        if (type == PlayerType.AI)
        {
            _aIDifficultySetting.gameObject.SetActive(true);
        }
        else
        {
            _aIDifficultySetting.gameObject.SetActive(false);
        }
    }

    public PlayerSetting GetInfo()
    {
        //find chosen model type
        _modelType = typeAndIcons.First(kvPair => kvPair.Value == _icon.sprite).Key;
        //set all parameters
        int difficulty = _playerType == PlayerType.AI ? _aIDifficultySetting.GetDifficulty() : -1;
        GameObject model = typeAndModels[_modelType];
        return new PlayerSetting((int)_modelType ,_playerType, _icon.sprite, model, difficulty);
    }
}
