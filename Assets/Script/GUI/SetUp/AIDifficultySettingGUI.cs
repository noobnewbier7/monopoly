﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AIDifficultySettingGUI : MonoBehaviour
{

    [SerializeField] List<Text> _correspondingText;
    [SerializeField] Slider _slider;

    private void Start()
    {
        ShowCorrespondingValue();
    }

    public void ShowCorrespondingValue()
    {
        foreach (Text text in _correspondingText)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, 0);
        }
        _correspondingText[(int)_slider.value - 1].color = new Color(
            _correspondingText[(int)_slider.value - 1].color.r,
            _correspondingText[(int)_slider.value - 1].color.g,
            _correspondingText[(int)_slider.value - 1].color.b, 1);
    }

    public int GetDifficulty()
    {
        return (int)_slider.value;
    }

}
