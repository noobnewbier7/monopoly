﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// A customized radio button that only support 2 choice(Human Or AI), maybe I should make it a bit more generic
/// </summary>
public class PlayerTypeButton : MonoBehaviour {

    [SerializeField] PlayerSettingGUI _correspongdingPlayerSetting;

    [SerializeField] Image _choiceHuman;
    [SerializeField] Sprite _choiceHumanOnImage;
    [SerializeField] Sprite _choiceHumanOffImage;
    [SerializeField] Image _choiceAI;
    [SerializeField] Sprite _choiceAIOnImage;
    [SerializeField] Sprite _choiceAIOffImage;

    public void ChooseHuman()
    {
        _correspongdingPlayerSetting.SetPlayerType(PlayerType.Human);
        _choiceHuman.sprite = _choiceHumanOnImage;
        _choiceAI.sprite = _choiceAIOffImage;
    }

    public void ChooseAI()
    {
        _correspongdingPlayerSetting.SetPlayerType(PlayerType.AI);
        _choiceHuman.sprite = _choiceHumanOffImage;
        _choiceAI.sprite = _choiceAIOnImage;
    }
}
