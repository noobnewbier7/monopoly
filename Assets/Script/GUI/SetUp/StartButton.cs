﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartButton : Button
{
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        Debug.Log("ClickedOnStart");
        if (SetUpCanvasManager.Instance.SetInfo())
        {
            SceneManager.LoadSceneAsync("someTest");
        }
    }
}
