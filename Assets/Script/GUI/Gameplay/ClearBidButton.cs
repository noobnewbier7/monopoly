﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClearBidButton : Button{
    [SerializeField] InputField _amounText;
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        //reset the bid amount
        _amounText.text = "";
    }
}
