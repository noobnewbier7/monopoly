﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TradeCardButton : Button{
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        Player playerToTrade = GameManager.Instance.Players.Find(p => p.PlayerSetting.PlayerNo == transform.GetSiblingIndex());
        TradeManager.Instance.BeginTrade(GameManager.Instance.CurrentPlayer, playerToTrade);
        GameplayCanvasManager.Instance.RefreshTradeProcess();
    }
}
