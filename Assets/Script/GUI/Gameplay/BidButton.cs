﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

//for custom editor
[Serializable]
public class BidButton : Button
{

    [SerializeField] InputField _amountText;

    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        AuctionManager.Instance.MakeBid(Int32.Parse(_amountText.text));
        _amountText.text = "";
    }

}
