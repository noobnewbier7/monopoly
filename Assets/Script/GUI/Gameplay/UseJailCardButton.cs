﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UseJailCardButton : Button
{
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        JailPassCard card = GameManager.Instance.CurrentPlayer.Properties.Find(c => c.GetType() == typeof(JailPassCard)) as JailPassCard;
        card.Trigger(GameManager.Instance.CurrentPlayer);
    }

}
