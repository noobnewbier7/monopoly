﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//Make the preview card visible on hover and change the sprite accordingly
public class PreviewOnHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    const float TIME_TO_SHOW = 2f;
    [SerializeField] Image previewCard;
    // ok lets forget the fact that this way we have 5000000 references of preview card
    bool _isTiming;
    float _timer = 0;

    private void Update()
    {
        if (_isTiming)
        {
            _timer += Time.deltaTime;
        }
        if (_timer > TIME_TO_SHOW && !previewCard.gameObject.activeSelf)
        {
            ShowPreviewCard();
        }
    }
    void ShowPreviewCard()
    {
        previewCard.sprite = GetComponent<Image>().sprite;
        previewCard.gameObject.SetActive(true);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _isTiming = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //reset timer and hide preview card
        _isTiming = false;
        _timer = 0;
        previewCard.gameObject.SetActive(false);
    }
}
