﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SubmitOfferButton : Button
{

    [SerializeField] Transform _playerAProperties;
    [SerializeField] Transform _playerBProperties;


    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);

        List<IPurchasable> playerAOffer = new List<IPurchasable>();
        List<IPurchasable> playerBOffer = new List<IPurchasable>();

        foreach (Transform child in _playerAProperties)
        {
            if (child.Find("CheckBox").Find("Check").gameObject.activeSelf) // if the player checked it
                playerAOffer.Add(GameplayCanvasManager.Instance.PlayerATileReference[child.gameObject]);
        }

        foreach (Transform child in _playerBProperties)
        {
            if (child.Find("CheckBox").Find("Check").gameObject.activeSelf)
                playerAOffer.Add(GameplayCanvasManager.Instance.PlayerBTileReference[child.gameObject]);
        }

        TradeManager.Instance.PlayerAOffer.AddRange(playerAOffer);
        TradeManager.Instance.PlayerBOffer.AddRange(playerBOffer);
    }
}
