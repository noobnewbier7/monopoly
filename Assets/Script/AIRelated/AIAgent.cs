﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AIAgent : MonoBehaviour
{

    public Player Player { private get; set; }

    private void Act(List<Action> possibleActions)
    {
        System.Random rand = new System.Random();
        //choose action from all possible action (do nothing is possible?)
        Action selectedAction = possibleActions[rand.Next(possibleActions.Count)];
        //call that method
        selectedAction();
    }


    /// <summary>
    /// Find some possible actions and act according to the current state of statemachine
    /// </summary>
    /// <param name="currentState">current state of statemachie</param>
    public void MakeDecision(DecisionStateEnum currentState)
    {
        List<Action> possibleActions = new List<Action>();
        switch (currentState)
        {
            case DecisionStateEnum.RollDice:
                //sincerely you can only roll a dice, nothing else
                possibleActions.Add(() => GameManager.Instance.RollDice());
                break;
            case DecisionStateEnum.BuyEvent: // k this is the hard part
                //I think the only event for now is buying or I might be wrong.....
                PurchasableTile tile = (PurchasableTile)GameManager.Instance.CurrentPlayer.StandOn;
                if (tile.Cost < GameManager.Instance.CurrentPlayer.Money)
                {
                    possibleActions.Add(() => GameManager.Instance.Buy());
                }
                else
                {
                    possibleActions.Add(() => AuctionManager.Instance.StartAuction());
                }
                break;
            case DecisionStateEnum.BidEvent:
                //bit with $0, yeaaaaaaa
                possibleActions.Add(() => AuctionManager.Instance.MakeBid(0));
                break;
            case DecisionStateEnum.BusinessDecision:
                //k I don't know what you should do but lets say you don't wanna do anything for now
                possibleActions.Add(() => GameManager.Instance.AllDecisionMade());
                break;
            case DecisionStateEnum.JailDecision:
                //just roll a dice, bribing and using a card is against the law of the Noob Kingdom
                possibleActions.Add(() => GameManager.Instance.RollDice());
                break;
        }
        Act(possibleActions);
    }
}
