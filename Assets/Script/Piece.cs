﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour
{

    [SerializeField] private Vector3 _offSetFromBoard;
    [SerializeField] private float _speed;
    //temp...
    public Player Represent { get; set; }

    // Use this for initialization
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (Represent == null || Represent.StandOn == null)
            return;
        if (transform.position != Represent.StandOn.transform.position + _offSetFromBoard)
        {
            transform.position = Vector3.MoveTowards(transform.position, Represent.StandOn.transform.position + _offSetFromBoard, Time.deltaTime * _speed);
        }
    }

    public void Goto(Tile dest)
    {
        if (Represent.StandOn != null)
            Represent.StandOn.Emmision(false);
        Represent.StandOn = dest;
        //dest.Trigger(Represent);
        transform.position = dest.transform.position + _offSetFromBoard;
        dest.Emmision(true);
    }
}
