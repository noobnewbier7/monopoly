﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bank : MonoBehaviour, IEntity
{
    //singleton design -- in no case there will be more than one bank
    public static Bank Instance { get; private set; }
    public Dictionary<Player, List<IPurchasable>> Mortgage { get; private set; }
    //Assumming the bank own all property at the start
    public int Money { get { return _money; } set { _money = value; } }

    public List<IPurchasable> Properties { get; set; }
    private int _hotelNo = 12;
    private int _houseNo = 32;
    private int _money = 500000;
    private int _accumulatedFines;

    private void Awake()
    {
        Debug.Log("waking");
        if (Instance == null)
        {
            Instance = this;
            Mortgage = new Dictionary<Player, List<IPurchasable>>();
            Properties = new List<IPurchasable>();
        }
        else
        {
            Destroy(this);
        }
    }

    // Use this for initialization
    IEnumerator Start()
    {
        Debug.Log("Setting up managers");
        yield return new WaitUntil(() => !GameManager.Instance.IsSettingUp);
        //propagate the dict
        GameManager.Instance.Players.ForEach(p => Mortgage[p] = new List<IPurchasable>());
    }

    public void MakeMortgage(Player player, IPurchasable property)
    {
        player.Money += property.Cost / 2;
        player.Properties.Remove(property);
        Mortgage[player].Add(property);
    }

    public void Purchase(Player player, IPurchasable property)
    {
        Purchase(player, property, property.Cost);
    }

    //can be called by auction manager
    public void Purchase(Player player, IPurchasable property, int price)
    {
        if (property.Owner != null)
        {
            throw new System.InvalidOperationException("This property already has an owner");
        }
        else if (player.Money < price)
        {
            throw new System.InvalidOperationException("The player does not have enough money to buy this");
        }
        property.Owner = player;
        player.Properties.Add(property);
        player.Money -= price;
    }

    //always sell to the bank atm
    public void Sell(Player player, IPurchasable property)
    {
        if (!player.Properties.Remove(property)) // return false if property not found
        {
            throw new System.InvalidOperationException("The player does not own that property");
        }
        property.Owner = null;
        player.Money += property.Cost;
        Properties.Add(property);
    }

    /// <summary>
    /// getting the accumulated fines thus far, resetting the accumulated fines and the amount of property owned
    /// </summary>
    /// <returns></returns>
    public int CollectFines()
    {
        int temp = _accumulatedFines;
        Money -= _accumulatedFines;
        _accumulatedFines = 0;
        return temp;
    }

    public void ReceiveFines(Player player, int amount)
    {
        _money += amount;
        player.Money -= amount;
        _accumulatedFines += amount;
    }

    public void GiveOutMoney(Player player, int amount)
    {
        _money -= amount;
        player.Money += amount;
    }

    public void Invest(Land land)
    {
        if(land.HouseNo == 3) //building house or building hotel?
        {
            _hotelNo--;
        }
        else
        {
            _houseNo--;
        }
        land.Owner.Money -= land.GetInvestPrice();
        land.Build();
    }
}
