﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// again, doubt if this is needed
/// </summary>
public interface IEntity{
    int Money { get; set; }
    List<IPurchasable> Properties { get; set; }
}
