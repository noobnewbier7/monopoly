﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
/// <summary>
/// provide utilities for list, can be easily extended
/// </summary>
public static class ListUtility {
    
    public static void Shuffle<T>(ref List<T> list)
    {
        
        System.Random random = new System.Random();
        list.OrderBy(item => random.Next());
    }

    public static void Swap<T>(ref List<T> list, int i, int j)
    {
        T temp = list[i];
        list[i] = list[j];
        list[j] = temp;
    }
}
