﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// holding information, so selection of players are preserved between scenes
/// </summary>
public static class GameOptionHolder{

    #region fields
    private static int _abridgeTurn; //-1 for no abirdge
    private static bool _isAllowTrade;
    private static List<PlayerSetting> _playerSettings;
    #endregion
    #region properties
    public static int AbridgeTurn { get { return _abridgeTurn; }  set { _abridgeTurn = value; } }
    public static bool IsAllowTrade { get { return _isAllowTrade; } set { _isAllowTrade = value; } }
    public static List<PlayerSetting> PlayerSettings { get { return _playerSettings; } set { _playerSettings = value; } }
    #endregion
}
