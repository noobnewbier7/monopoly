﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckData : ScriptableObject {

    public List<CardData> CardDatas { get { return _cardDatas; } }
    [SerializeField] List<CardData> _cardDatas;

    //serve as a wrapper as properties could not be used as a reference parameter
    public void ShuffleDeck()
    {
        ListUtility.Shuffle(ref _cardDatas);
    }
}
