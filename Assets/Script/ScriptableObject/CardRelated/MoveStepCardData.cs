﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveStepCardData : CardData{

    public int Step { get { return _step; } }

    // negative for moving backwards
    [SerializeField] int _step;

    public override Card CreateCard()
    {
        return new MoveStepCard(this);
    }
}
