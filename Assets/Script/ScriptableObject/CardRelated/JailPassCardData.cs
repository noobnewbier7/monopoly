﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JailPassCardData : CardData
{
    public override Card CreateCard()
    {
        return new JailPassCard(this);
    }
}
