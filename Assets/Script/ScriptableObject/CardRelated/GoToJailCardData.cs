﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToJailCardData : CardData
{
    public override Card CreateCard()
    {
        return new GoToJailCard(this);
    }
}
