﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyCardData : CardData {

    public int Amount { get { return _amount; } }
    [SerializeField] int _amount; // negative for fines

    public override Card CreateCard()
    {
        return new MoneyCard(this);
    }
}
