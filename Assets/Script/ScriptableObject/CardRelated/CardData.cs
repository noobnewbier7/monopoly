﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CardData : ScriptableObject
{
    public string Description { get { return _description; } }
    public Material Material { get { return _material; } }

    [SerializeField] protected string _description; // might not need this
    [SerializeField] protected Material _material;

    public abstract Card CreateCard();
}
