﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstateMoneyCardData : CardData
{
    public int AmountPerHotel { get { return _amountPerHotel; } }
    public int AmountPerHouse { get { return _amountPerHouse; } }
    //negative for fines, positive for rewards
    [SerializeField] int _amountPerHotel;
    [SerializeField] int _amountPerHouse;

    public override Card CreateCard()
    {
        return new EstateMoneyCard(this);
    }
}
