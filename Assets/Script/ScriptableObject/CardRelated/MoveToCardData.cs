﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    Forward,
    BackWard
}

public class MoveToCardData : CardData
{
    public TileData Destination { get { return _destination; } }
    public Direction Direction { get { return _direction; } }
    [SerializeField] Direction _direction;
    [SerializeField] TileData _destination;

    public override Card CreateCard()
    {
        return new MoveToCard(this);
    }
}
