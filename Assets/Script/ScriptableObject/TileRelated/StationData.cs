﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationData : PurchasableData{
    public override TileType TileType
    {
        get { return TileType.station; }
    }
    public override Tile AttachToGameObject(GameObject tileGameObject)
    {
        Tile t = tileGameObject.AddComponent<Station>();
        t.TileData = this;
        return t;
    }
}
