﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandData : PurchasableData{

    public override TileType TileType
    {
        get
        {
            return TileType.land;
        }
    }
    public PropertyColor PropertyColor { get { return PropertyColor; } }
    public Rent Rent { get { return rent; } }
    public HouseCost HouseCost { get { return houseCost; } }
    [SerializeField] protected Rent rent;
    [SerializeField] protected HouseCost houseCost;
    [SerializeField] protected PropertyColor propertyColor;

    public override Tile AttachToGameObject(GameObject tileGameObject)
    {
        Tile t = tileGameObject.AddComponent<Land>();
        t.TileData = this;
        return t;
    }
}
