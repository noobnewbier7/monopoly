﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoData : NonPurchasableData{
    public override TileType TileType
    {
        get
        {
            return TileType.go;
        }
    }
    public override Tile AttachToGameObject(GameObject tileGameObject)
    {
        Tile t = tileGameObject.AddComponent<Go>();
        t.TileData = this;
        return t;
    }
}
