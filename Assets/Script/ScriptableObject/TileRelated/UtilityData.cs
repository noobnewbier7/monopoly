﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilityData : PurchasableData{

    public override TileType TileType
    {
        get
        {
            return TileType.utilities;
        }
    }
    public override Tile AttachToGameObject(GameObject tileGameObject)
    {
        Tile t = tileGameObject.AddComponent<Utility>();
        t.TileData = this;
        return t;
    }
}
