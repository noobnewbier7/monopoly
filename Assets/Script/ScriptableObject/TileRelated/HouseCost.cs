﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseCost : ScriptableObject {
    public List<int> CostLevel { get { return _costLevel; } }
    [SerializeField] List<int> _costLevel;
}
