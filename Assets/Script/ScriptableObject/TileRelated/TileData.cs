﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//add the rest urself -- Jamie
public enum TileType
{
    land,
    station,
    utilities,
    jail,
    freeParking,
    go,
    potLuck,
    opportunityKnock
}

//add the rest urself -- Jamie
public enum PropertyColor
{
    None,
    red,
    blue,
    brown,
    purple,
    orange,
    yellow,
    green
}

public abstract class TileData : ScriptableObject
{
    #region Property
    public abstract TileType TileType { get; }
    public abstract bool Purchasable { get; }
    public string TileName { get { return tileName; } }
    public Material Material { get { return material; } }
    public Sprite Sprite { get { return sprite; } }
    #endregion

    #region Serialize Field
    [SerializeField] protected string tileName;
    [SerializeField] protected Material material;
    [SerializeField] protected Sprite sprite;
    #endregion


    /// <summary>
    /// Attach itselfs to a GameObject
    /// </summary>
    /// <returns>an instance of a tile representing this tile data</returns>
    public abstract Tile AttachToGameObject(GameObject tileGameObject);
}

