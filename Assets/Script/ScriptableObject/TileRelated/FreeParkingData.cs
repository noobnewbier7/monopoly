﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeParkingData : NonPurchasableData {
    public override TileType TileType
    {
        get
        {
            return TileType.freeParking;
        }
    }

    public override Tile AttachToGameObject(GameObject tileGameObject)
    {
        Tile t = tileGameObject.AddComponent<FreeParking>();
        t.TileData = this;
        return t;
    }
}
