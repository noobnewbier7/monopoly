﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PurchasableData : TileData
{
    public override bool Purchasable
    {
        get
        {
            return true;
        }
    }
    public int Cost { get { return cost; } }

    [SerializeField] protected int cost;

}
