﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardData : ScriptableObject
{
    public List<TileData> TileDatas { get { return _tileDatas; } }
    [SerializeField] private List<TileData> _tileDatas;
}
