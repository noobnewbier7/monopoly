﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JailData : NonPurchasableData{
    [SerializeField] Vector3 _visitAreaOffset;
    public Vector3 VisitAreaOffset { get { return _visitAreaOffset; } }
    public override TileType TileType
    {
        get
        {
            return TileType.jail;
        }
    }
    public override Tile AttachToGameObject(GameObject tileGameObject)
    {
        Tile t = tileGameObject.AddComponent<Jail>();
        t.TileData = this;
        return t;
    }
}
