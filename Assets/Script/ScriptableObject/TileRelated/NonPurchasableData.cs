﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class NonPurchasableData : TileData{
    public override bool Purchasable
    {
        get
        {
            return false;
        }
    }
}
