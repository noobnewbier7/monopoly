﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotLuckData : NonPurchasableData
{
    public override TileType TileType
    {
        get
        {
            return TileType.potLuck;
        }
    }

    public override Tile AttachToGameObject(GameObject tileGameObject)
    {
        Tile t = tileGameObject.AddComponent<PotLuck>();
        t.TileData = this;
        return t;
    }
}
