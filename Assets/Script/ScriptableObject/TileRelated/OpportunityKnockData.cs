﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpportunityKnockData : NonPurchasableData
{
    public override TileType TileType
    {
        get
        {
            return TileType.opportunityKnock;
        }
    }

    public override Tile AttachToGameObject(GameObject tileGameObject)
    {
        Tile t = tileGameObject.AddComponent<OpportunityKnock>();
        t.TileData = this;
        return t;
    }
}
