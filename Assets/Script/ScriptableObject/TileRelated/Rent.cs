﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rent : ScriptableObject
{
    private const int MAX_LEVEL = 5;
    public int[] RentLevel { get { return _rentLevel; } }

    [SerializeField] private int[] _rentLevel;

    private void Awake()
    {
        Debug.Assert(_rentLevel.Length == MAX_LEVEL);
    }
}
