﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiddingState : StateMachineBehaviour{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        if (AuctionManager.Instance.CurrentPlayer.PlayerSetting.PlayerType == PlayerType.AI)
        {
            AuctionManager.Instance.CurrentPlayer.Piece.GetComponent<AIAgent>().MakeDecision(DecisionStateEnum.BidEvent);
        }
    }
}
