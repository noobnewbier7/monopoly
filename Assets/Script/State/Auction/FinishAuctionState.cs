﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishAuctionState : StateMachineBehaviour{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        AuctionManager.Instance.FinishAuction();
        animator.SetTrigger("WrapUp");
    }
}
