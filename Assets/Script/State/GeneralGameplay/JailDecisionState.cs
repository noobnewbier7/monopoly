﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JailDecisionState : StateMachineBehaviour{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        JailManager.Instance.NewTurn(GameManager.Instance.CurrentPlayer);
        if (GameManager.Instance.CurrentPlayer.PlayerSetting.PlayerType == PlayerType.AI) // if it is an AI
        {
            AIAgent agent = GameManager.Instance.CurrentPlayer.Piece.GetComponent<AIAgent>();
            agent.MakeDecision(DecisionStateEnum.JailDecision);
        }
    }
   
}
