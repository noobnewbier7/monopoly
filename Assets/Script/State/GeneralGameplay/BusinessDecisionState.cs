﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusinessDecisionState : StateMachineBehaviour{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        GameplayCanvasManager.Instance.RefreshDefault();
        if (GameManager.Instance.CurrentPlayer.PlayerSetting.PlayerType == PlayerType.AI) // if it is an AI
        {
            AIAgent agent = GameManager.Instance.CurrentPlayer.Piece.GetComponent<AIAgent>();
            agent.MakeDecision(DecisionStateEnum.BusinessDecision);
        }
    }
}
