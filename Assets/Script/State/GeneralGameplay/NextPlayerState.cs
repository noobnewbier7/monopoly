﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextPlayerState : StateMachineBehaviour{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        GameplayCanvasManager.Instance.DeactivateAll();
        GameManager.Instance.NextPlayer();
    }
}
