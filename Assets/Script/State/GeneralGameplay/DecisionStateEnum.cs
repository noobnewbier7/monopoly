﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// All states that requires input(decision)
/// </summary>
public enum DecisionStateEnum{
    RollDice,
    BuyEvent,
    BidEvent,
    BusinessDecision,
    JailDecision
}
