﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceForJailState : StateMachineBehaviour{
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        if (GameManager.Instance.IsDouble)
        {
            JailManager.Instance.Release(GameManager.Instance.CurrentPlayer);
        }
    }
}
