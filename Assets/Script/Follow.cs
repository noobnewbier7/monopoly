﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Modified so it always follow the current player
/// </summary>
public class Follow : MonoBehaviour {

    [SerializeField] private float distanceAway;
    [SerializeField] private float distanceUp;
    [SerializeField] private float smooth;
    [SerializeField] private Transform follow;
    [SerializeField] private Transform playerObject;
    private Vector3 targetPosition;


    // Update is called once per frame
    void Update () {



        if(GameManager.Instance.CurrentPlayer == null) // only do stuff when the current player is not null
        {
            return;
        }


        if ((BoardManager.Instance.Board.FindIndex(t => GameManager.Instance.CurrentPlayer.StandOn == t)) == 10 & GameManager.Instance.CurrentPlayer.Corner[0] == false )
        {
            StartCoroutine(RotateMe(Vector3.up * 90, 2f)); //Uses coroutine so it doesn't repeat the update on each frame and only repeats once completed.
            GameManager.Instance.CurrentPlayer.Corner[0] = true;
        }

        if((BoardManager.Instance.Board.FindIndex(t => GameManager.Instance.CurrentPlayer.StandOn == t)) == 11)
        {
            GameManager.Instance.CurrentPlayer.Corner[0] = false;
        }

        if ((BoardManager.Instance.Board.FindIndex(t => GameManager.Instance.CurrentPlayer.StandOn == t)) == 20 & GameManager.Instance.CurrentPlayer.Corner[1] == false)
        {
            StartCoroutine(RotateMe(Vector3.up * 90, 2f)); //Uses coroutine so it doesn't repeat the update on each frame and only repeats once completed.
            GameManager.Instance.CurrentPlayer.Corner[1] = true;
        }

        if ((BoardManager.Instance.Board.FindIndex(t => GameManager.Instance.CurrentPlayer.StandOn == t)) == 21)
        {
            GameManager.Instance.CurrentPlayer.Corner[1] = false;
        }

        if ((BoardManager.Instance.Board.FindIndex(t => GameManager.Instance.CurrentPlayer.StandOn == t)) == 30 & GameManager.Instance.CurrentPlayer.Corner[2] == false)
        {
            StartCoroutine(RotateMe(Vector3.up * 90, 2f)); //Uses coroutine so it doesn't repeat the update on each frame and only repeats once completed.
            GameManager.Instance.CurrentPlayer.Corner[2] = true;
        }

        if ((BoardManager.Instance.Board.FindIndex(t => GameManager.Instance.CurrentPlayer.StandOn == t)) == 31)
        {
            GameManager.Instance.CurrentPlayer.Corner[2] = false;
        }

        if ((BoardManager.Instance.Board.FindIndex(t => GameManager.Instance.CurrentPlayer.StandOn == t)) == 0 & GameManager.Instance.CurrentPlayer.Corner[3] == false)
        {
            StartCoroutine(RotateMe(Vector3.up * 90, 2f)); //Uses coroutine so it doesn't repeat the update on each frame and only repeats once completed.
            GameManager.Instance.CurrentPlayer.Corner[3] = true;
        }

        if ((BoardManager.Instance.Board.FindIndex(t => GameManager.Instance.CurrentPlayer.StandOn == t)) == 1)
        {
            GameManager.Instance.CurrentPlayer.Corner[3] = false;
        }

    }


    private void LateUpdate()   //LateUpdate because the camera needs to check and move after the object has moved in ubdate
    {
        //SOMETIMES THIS THROW NULL REFERENCE EXCEPTION
        if (GameManager.Instance.CurrentPlayer == null) // only do stuff when the current player is not null
        {
            return;
        }
        playerObject = GameManager.Instance.CurrentPlayer.Piece.transform;
        follow = playerObject.Find("Follow");
        targetPosition = follow.position + follow.up * distanceUp - follow.forward * distanceAway; // Debug stuff draws cool looking lines
        Debug.DrawRay(follow.position, Vector3.up * distanceUp, Color.red);
        Debug.DrawRay(follow.position, -1f * follow.forward * distanceAway, Color.blue); 
        Debug.DrawLine(follow.position, targetPosition, Color.magenta);

        transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * smooth);

        transform.LookAt(follow);
    }

    private IEnumerator RotateMe(Vector3 byAngles, float inTime) //maths
    {
        var fromAngle = playerObject.rotation;
        var toAngle = Quaternion.Euler(playerObject.eulerAngles + byAngles); //Basically checks the starting angle of the camera and adds a number of degrees to that angle and moves the camera to that angle
        for (var t = 0f; t < 1; t += Time.deltaTime / inTime)
        {
            playerObject.rotation = Quaternion.Slerp(fromAngle, toAngle, t);

            yield return null;
        }

    }
}
