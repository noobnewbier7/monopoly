﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ModelType { 
    Cat,
    Goblet,
    Phone,
    Spoon,
    TopHat,
    WorkBoot
}
