﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControler : MonoBehaviour
{

    public enum CameraState
    {
        StaticAndRotate,
        OverView
    }

    //singleton
    public static CameraControler Instance { get; private set; }
    public Piece Piece { get; set; }
    public CameraState State { private get { return _state; } set { _state = value; StartCoroutine(Transition()); } }

    //might not need this
    [SerializeField] private Vector3 _offSet;
    [SerializeField] private float _speed;
    [SerializeField] private Transform _centerTransform;
    [SerializeField] private Vector3 _offSetFromCenter;

    private CameraState _state = CameraState.OverView;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            transform.position = _centerTransform.position + _offSetFromCenter;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Update()
    {
        //currently just following the player
        //if (transform.position != Piece.transform.position + _offSet)
        //{
        //    transform.position = Vector3.MoveTowards(transform.position, Piece.transform.position + _offSet, Time.deltaTime * _speed);
        //}

        if (_state == CameraState.OverView)
        {
            OverView();
        }
        else
        {
            LookAtPiece();
        }
    }

    void LookAtPiece()
    {
        transform.LookAt(GameManager.Instance.CurrentPlayer.Piece.transform);
    }

    void OverView()
    {
        //do nothing at the moment;
    }

    IEnumerator Transition()
    {
        Vector3 dest = _state == CameraState.OverView ? _centerTransform.position + _offSetFromCenter : _centerTransform.position + _offSet;
        while(transform.position != dest)
        {
            yield return new WaitForEndOfFrame();
            transform.position = Vector3.Lerp(transform.position, dest, _speed * Time.deltaTime);
        }
        yield break;
    }
}
