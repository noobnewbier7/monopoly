﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuyButton : Button {

    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        Bank.Instance.Purchase(GameManager.Instance.CurrentPlayer, (PurchasableTile)GameManager.Instance.CurrentPlayer.StandOn);
    }
}
