﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// wonder if I should implement the cost thing here...
/// </summary>
public abstract class PurchasableTile : Tile, IPurchasable
{
    public abstract int Cost { get; }

    IEntity IPurchasable.Owner
    {
        get
        {
            return Owner;
        }

        set
        {
            Owner = value;
        }
    }

    public void SellToBank()
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// might not be needed....
    /// </summary>
    /// <param name="buyer"></param>
    public void SellToPlayer(Player buyer)
    {
        Owner.Money += Cost;
        buyer.Money -= Cost;
    }
}
