﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface IPurchasable
{
    IEntity Owner { get; set; }
    int Cost { get; }

    void SellToPlayer(Player player);
    void SellToBank();
}
