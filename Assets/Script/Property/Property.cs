﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Property : MonoBehaviour
{
    public IEntity Owner { get; set; }
}
