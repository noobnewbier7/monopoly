﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : PurchasableTile
{

    private UtilityData _utilityData;

    public override TileData TileData
    {
        get
        {
            return _utilityData;
        }

        set
        {
            _utilityData = (UtilityData)value;
        }
    }

    public override int Cost
    {
        get
        {
            return _utilityData.Cost;
        }
    }

    public override void Trigger(Player player)
    {
        if (Owner != null && Owner != player)
        {
            // 10 if owner have 2 util, 4 if only 1 util
            int scale = (Owner.Properties.FindAll(p => p.GetType() == typeof(Utility)).Count == 2) ? 10 : 4;
            player.Pay(Owner, GameManager.Instance.Dice * scale);
            GameManager.Instance.EventFinished();
        }
        else
        {
            GameplayCanvasManager.Instance.RefreshBuyPrompt(TileData.Sprite, TileData.TileName, _utilityData.Cost);
            // if our dear current player is a noob AI
            if (GameManager.Instance.CurrentPlayer.PlayerSetting.PlayerType == PlayerType.AI)
            {
                GameManager.Instance.CurrentPlayer.Piece.GetComponent<AIAgent>().MakeDecision(DecisionStateEnum.BuyEvent);
            }
        }
    }
}
