﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Land : PurchasableTile
{
    // having 5 house means it is a hotel
    public int HouseNo { get {return _houseNo; } }
    public int HotelNo { get { return _hotelNo; } }
    int _houseNo = 0;
    int _hotelNo = 0;
    List<Transform> _housesTransform;

    private LandData _landData;

    public override TileData TileData
    {
        get
        {
            return _landData;
        }

        set
        {
            _landData = (LandData)value;
        }
    }

    public override int Cost
    {
        get
        {
            return _landData.Cost;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        _housesTransform = new List<Transform>(transform.GetComponentsInChildren<Transform>());
    }

    public override void Trigger(Player player)
    {
        if (Owner != null && Owner != player)
        {
            int amount = _landData.Rent.RentLevel[_houseNo];
            player.Pay(Owner, amount);
            GameManager.Instance.EventFinished();
        }
        else
        {
            GameplayCanvasManager.Instance.RefreshBuyPrompt(TileData.Sprite, TileData.TileName, _landData.Cost);
            // if our dear current player is a noob AI
            if (GameManager.Instance.CurrentPlayer.PlayerSetting.PlayerType == PlayerType.AI)
            {
                GameManager.Instance.CurrentPlayer.Piece.GetComponent<AIAgent>().MakeDecision(DecisionStateEnum.BuyEvent);
            }
        }
    }

    public void Build() // hotel anim not implemented
    {
        _houseNo++;
        _housesTransform[_houseNo - 1].gameObject.SetActive(true);
        // if has a hotel
        if (_houseNo == 4)
        {
            _hotelNo = 0;
            _hotelNo = 1;
        }
    }

    public void UnBuild()
    {
        if (_houseNo == 0)
        {
            _houseNo = 4;
            _hotelNo = 0;
        }
        else
        {
            _houseNo--;
        }
        _housesTransform[_houseNo + 1].gameObject.SetActive(false);
    }

    public int GetInvestPrice()
    {
        return _landData.HouseCost.CostLevel[_houseNo];
    }
}
