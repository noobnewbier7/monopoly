﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Station : PurchasableTile
{

    private StationData _stationData;
    public override TileData TileData
    {
        get
        {
            return _stationData;
        }

        set
        {
            _stationData = (StationData)value;
        }
    }
    public override int Cost
    {
        get
        {
            return _stationData.Cost;
        }
    }

    public override void Trigger(Player player)
    {
        if (Owner != null && Owner != player)
        {
            //considering using this formula instead of a switch
            //amount =  2 ^ (Owner.Properties.FindAll(p => p.PropertyType == PropertyType.station).Count - 1)
            int amount = 0;
            switch (Owner.Properties.FindAll(p => p.GetType() == typeof(Station)).Count)
            {
                case 1:
                    amount = 25;
                    break;
                case 2:
                    amount = 50;
                    break;
                case 3:
                    amount = 100;
                    break;
                case 4:
                    amount = 200;
                    break;
            }
            player.Pay(Owner, amount);
            GameManager.Instance.EventFinished();
        }
        else
        {
            GameplayCanvasManager.Instance.RefreshBuyPrompt(TileData.Sprite, TileData.TileName, _stationData.Cost);
            // if our dear current player is a noob AI
            if (GameManager.Instance.CurrentPlayer.PlayerSetting.PlayerType == PlayerType.AI)
            {
                GameManager.Instance.CurrentPlayer.Piece.GetComponent<AIAgent>().MakeDecision(DecisionStateEnum.BuyEvent);
            }
        }
    }
}
