﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Tile : Property, IEvent
{
    public TileType TileType { get { return TileData.TileType; } }
    public virtual TileData TileData
    {
        get;
        set;
    }

    private Material _material;

    protected virtual void Awake()
    {
    }

    public abstract void Trigger(Player player);

    /// <summary>
    /// setting the tile data while refreshing the gui
    /// </summary>
    /// <param name="tileData"></param>
    public void SetTileData(TileData tileData)
    {
        TileData = tileData;
        RefreshGUI();
    }

    public void Emmision(bool on)
    {
        Color color = on? Color.red : Color.black;
        _material.SetColor("_EmissionColor", color);
    }

    private void RefreshGUI()
    {
        transform.Find("Cube-Visual").GetComponent<Renderer>().material = Instantiate(TileData.Material);
        _material = transform.Find("Cube-Visual").GetComponent<Renderer>().material;
    }
}
