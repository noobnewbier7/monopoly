﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jail : NonPurchasableTile
{
    public List<Player> _players = new List<Player>();

    private JailData _jailData;

    public override TileData TileData
    {
        get
        {
            return _jailData;
        }

        set
        {
            _jailData = (JailData)value;
        }
    }

    public override void Trigger(Player player)
    {
        //seriously, nothing happen when you are just visiting, so we only move your to the visit area
        player.Piece.transform.Translate(_jailData.VisitAreaOffset + transform.position);
        GameManager.Instance.EventFinished();
        return;
    }

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
    }
}
