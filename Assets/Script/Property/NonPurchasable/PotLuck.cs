﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotLuck : NonPurchasableTile
{

    private PotLuckData _potLuckData;

    public override TileData TileData
    {
        get
        {
            return _potLuckData;
        }

        set
        {
            _potLuckData = (PotLuckData)value;
        }
    }

    public override void Trigger(Player player)
    {
        Card card = CardManager.Instance.DrawACard(CardType.PotLuck);
        card.Trigger(player);
        GameManager.Instance.EventFinished();
    }
}
