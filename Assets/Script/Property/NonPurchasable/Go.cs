﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Not to be confused with GO which stands for "Game Object", it is the starting point of the game.
/// </summary>
public class Go : NonPurchasableTile
{
    private GoData _goData;
    public override TileData TileData
    {
        get
        {
            return _goData;
        }

        set
        {
            _goData = (GoData)value;
        }
    }
    public override void Trigger(Player player)
    {
        Bank.Instance.GiveOutMoney(player, GameManager.Instance.GoBonus);
        GameManager.Instance.EventFinished();
    }
}
