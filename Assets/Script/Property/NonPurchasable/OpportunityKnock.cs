﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpportunityKnock : NonPurchasableTile
{
    private OpportunityKnockData _opportunityKnockData;

    public override TileData TileData
    {
        get
        {
            return _opportunityKnockData;
        }

        set
        {
            _opportunityKnockData = (OpportunityKnockData)value;
        }
    }

    public override void Trigger(Player player)
    {
        Card card = CardManager.Instance.DrawACard(CardType.Opportunity);
        card.Trigger(player);
        GameManager.Instance.EventFinished();
    }
}