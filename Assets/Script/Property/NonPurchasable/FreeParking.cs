﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeParking : NonPurchasableTile
{

    private FreeParkingData _freeParkingData;

    public override TileData TileData
    {
        get
        {
            return _freeParkingData;
        }

        set
        {
            _freeParkingData = (FreeParkingData)value;
        }
    }

    public override void Trigger(Player player)
    {
        player.Money += Bank.Instance.CollectFines();
        GameManager.Instance.EventFinished();
    }
}
