﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : IEntity
{

    int _money;
    public int Money { get { return _money; } set { _money = value; } }
    public PlayerSetting PlayerSetting { get; private set; }
    public List<IPurchasable> Properties { get; set; }
    public Tile StandOn { get; set; }
    public Piece Piece { get; set; }
    public bool[] Corner = new bool[4];
    


    public Player(PlayerSetting playerSetting)
    {
        Properties = new List<IPurchasable>();
        PlayerSetting = playerSetting;
        Corner[0] = false;
        Corner[1] = false;
        Corner[2] = false;
        Corner[3] = true;
    }

    public void Pay(IEntity payee, int amount)
    {
        _money -= amount;
        payee.Money += amount;
    }
}
