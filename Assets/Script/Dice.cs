﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour
{
    [SerializeField] float _forwardForce;
    [SerializeField] float _rotateForce;
    [SerializeField] float _randomForce;
    Rigidbody _rigidbody;
    Vector3 _origin;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.useGravity = false;
        _origin = transform.position;
    }

    public void ResetPlace()
    {
        transform.position = _origin;
    }

    //essentially returning a value by modifying the reference, a bit of a cheeky approach
    //could not use keyword ref as it is returning IEnumerator
    public IEnumerator Throw(System.Action<int> result)
    {
        Coroutine c = StartCoroutine(PhysicallyThrow());
        yield return c; //wait till coroutines are finished
        result(DiceValue()); // "returning" stuff
    }

    IEnumerator PhysicallyThrow()
    {
        //randoming a vector3 for torque
        _rigidbody.useGravity = true;
        float variation = Random.Range(0, _randomForce);
        Vector3 torque = new Vector3(Random.Range(-variation, variation), Random.Range(-variation, variation), Random.Range(-variation, variation));
        _rigidbody.AddForce(transform.forward * _forwardForce * variation, ForceMode.Impulse);
        _rigidbody.AddRelativeTorque(torque * _rotateForce * Random.Range(-variation, variation));
        yield return new WaitUntil(() => _rigidbody.IsSleeping());
        _rigidbody.useGravity = false;
        Debug.Log("finished");
        //find which face is top
    }

    int DiceValue() // wonder if those if statement could be reduced
    {
        List<Vector3> directions = new List<Vector3> { transform.up, -transform.up, transform.forward, -transform.forward, transform.right, -transform.right };
        Vector3 minFace = new Vector3();
        float minAngle = 999f;
        foreach (Vector3 face in directions)
        {
            float angle = Vector3.Angle(Vector3.up, face);
            if (angle < minAngle)
            {
                minAngle = angle;
                minFace = face;
            }
        }
        //return value depends on the face up
        if (minFace == transform.up)
        {
            return 1;
        }
        else if (minFace == -transform.up)
        {
            return 6;
        }
        else if (minFace == transform.forward)
        {
            return 3;
        }
        else if (minFace == -transform.forward)
        {
            return 4;
        }
        else if (minFace == transform.right)
        {
            return 5;
        }
        //else if (minFace == -transform.right)
        return 2;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
