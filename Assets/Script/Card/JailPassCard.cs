﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JailPassCard : Card, IPurchasable{

    private JailPassCardData _jailPassCardData;

    public JailPassCard(JailPassCardData cardData) : base(cardData)
    {
    }

    public IEntity Owner
    {
        get;
        set;
    }
    //I honestly don't know what is the cost of it
    public int Cost { get { return 0; } }

    protected override CardData CardData
    {
        get
        {
            return _jailPassCardData;
        }

        set
        {
            _jailPassCardData = (JailPassCardData)value;
        }
    }

    public void SellToBank()//maybe no more selling to player or bank?
    {
        throw new System.NotImplementedException();
    }

    public void SellToPlayer(Player player) //maybe no more selling to player or bank?
    {
        throw new System.NotImplementedException();
    }

    public override void Trigger(Player player)
    {
        JailManager.Instance.UseCard(player);
        player.Properties.Remove(this);
    }

}
