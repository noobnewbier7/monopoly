﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToCard : Card
{
    private MoveToCardData _moveToCardData;

    public MoveToCard(MoveToCardData cardData) : base(cardData)
    {
    }

    protected override CardData CardData
    {
        get
        {
            return _moveToCardData;
        }

        set
        {
            _moveToCardData = (MoveToCardData)value;
        }
    }

    public override void Trigger(Player player)
    {
        GameManager.Instance.MovePiece(player, _moveToCardData.Destination, _moveToCardData.Direction);
    }
}
