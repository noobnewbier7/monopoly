﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveStepCard : Card{

    private MoveStepCardData _moveStepCardData;
    protected override CardData CardData
    {
        get
        {
            return _moveStepCardData;
        }

        set
        {
            _moveStepCardData = (MoveStepCardData)value;
        }
    }

    public MoveStepCard(MoveStepCardData cardData) : base(cardData)
    {
    }

    public override void Trigger(Player player)
    {
        GameManager.Instance.MovePiece(player, _moveStepCardData.Step);
    }

}
