﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToJailCard : Card{
    private GoToJailCardData _goToJailCardData;

    public GoToJailCard(GoToJailCardData cardData) : base(cardData)
    {
    }

    protected override CardData CardData
    {
        get
        {
            return _goToJailCardData;
        }

        set
        {
            _goToJailCardData = (GoToJailCardData)value;
        }
    }

    public override void Trigger(Player player)
    {
        JailManager.Instance.SendToJail(player);
    }
}
