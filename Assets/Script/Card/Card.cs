﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Card : IEvent
{
    protected abstract CardData CardData { get; set; }

    public Card(CardData cardData)
    {
        CardData = cardData;
    }

    public abstract void Trigger(Player player);
}
