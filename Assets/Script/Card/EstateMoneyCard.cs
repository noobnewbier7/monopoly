﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EstateMoneyCard : Card
{
    private EstateMoneyCardData _estateMoneyCardData;

    public EstateMoneyCard(EstateMoneyCardData cardData) : base(cardData)
    {
    }

    protected override CardData CardData
    {
        get
        {
            return _estateMoneyCardData;
        }

        set
        {
            _estateMoneyCardData = (EstateMoneyCardData)value;
        }
    }

    public override void Trigger(Player player)
    {
        int moneyChange = 0;
        List<Land> allLand = player.Properties.FindAll(p => p.GetType() == typeof(Land)).Cast<Land>().ToList();
        foreach (Land land in allLand)
        {
            moneyChange += land.HouseNo * _estateMoneyCardData.AmountPerHouse + land.HotelNo * _estateMoneyCardData.AmountPerHotel;
        }
        player.Money += moneyChange;
    }
}
