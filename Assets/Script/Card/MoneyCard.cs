﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyCard : Card{

    private MoneyCardData _moneyCardData;

    public MoneyCard(MoneyCardData cardData) : base(cardData)
    {
    }

    protected override CardData CardData
    {
        get
        {
            return _moneyCardData;
        }

        set
        {
            _moneyCardData = (MoneyCardData)value;
        }
    }

    public override void Trigger(Player player)
    {
        player.Money += _moneyCardData.Amount;
    }

}
