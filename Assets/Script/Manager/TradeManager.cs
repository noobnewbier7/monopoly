﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TradeManager : MonoBehaviour {

    //singleton
    public static TradeManager Instance { get; private set; }

    private Player _playerA;
    private Player _playerB;

    public Player PlayerA { get { return _playerA; } }
    public Player PlayerB { get { return _playerB; } }

    public List<IPurchasable> PlayerAOffer { get; set; }
    public List<IPurchasable> PlayerBOffer { get; set; }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void BeginTrade(Player playerA, Player playerB)
    {
        _playerA = playerA;
        _playerB = playerB;
    }

    public void FinishTrade()
    {
        _playerA.Properties.AddRange(PlayerBOffer);
        _playerA.Properties.RemoveAll(prop =>PlayerAOffer.Contains(prop));

        _playerB.Properties.AddRange(PlayerAOffer);
        _playerB.Properties.RemoveAll(prop => PlayerBOffer.Contains(prop));

        GameplayCanvasManager.Instance.RefreshDefault();
    }
}
