﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AuctionManager : MonoBehaviour
{

    //singleton as always
    public static AuctionManager Instance { get; private set; }

    public Tile AuctionTile { get { return _auctionTile; } }
    public List<Player> PlayerInAuction
    {
        get
        {
            return GameManager.Instance.Players.Where(p => p != GameManager.Instance.CurrentPlayer).
            Intersect(GameManager.Instance.PlayersFinishedFirstLap).ToList();
        }
    }
    public Player CurrentPlayer { get { return _playerInAuction.Current; } }

    private Tile _auctionTile; // the tile being "auctioning" if that is a word.... if thats not a word than it is now
    private Player _highestPricePlayer;
    private int _highestPrice;
    private IEnumerator<Player> _playerInAuction;
    private Animator _statemachine;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            _statemachine = GetComponent<Animator>();
        }
        else
        {
            Destroy(this);
        }
    }

    // Use this for initialization
    public void StartAuction(PurchasableTile tile)
    {
        _auctionTile = tile;
        //all player finished the first lap (excluding the current player)
        List<Player> playerInAuction = GameManager.Instance.Players.Where(p => p != GameManager.Instance.CurrentPlayer).
            Intersect(GameManager.Instance.PlayersFinishedFirstLap).ToList();
        if (playerInAuction.Count < 2)
        {
            Debug.Log(playerInAuction.Count + " player is not enough for an auction");
            GameManager.Instance.EventFinished();
            return;
        }
        _playerInAuction = playerInAuction.GetEnumerator();
        _playerInAuction.MoveNext();
        _statemachine.SetTrigger("StartAuction");
        GameplayCanvasManager.Instance.ShowAuction();
    }
    //an override for gui which automatically takes the tile the current player stand on
    public void StartAuction()
    {
        StartAuction((PurchasableTile)GameManager.Instance.CurrentPlayer.StandOn);
    }

    //return true if there are next player waiting for a bid
    public void MakeBid(int offer)
    {
        Debug.Log("making bid");
        _highestPricePlayer = _playerInAuction.Current;
        _highestPrice = offer;
        if (!_playerInAuction.MoveNext())
        {
            _statemachine.SetTrigger("HaveMorePlayer");
        }
        else
        {
            _statemachine.SetTrigger("AllDone");
        }
    }

    public void FinishAuction()
    {
        Bank.Instance.Purchase(_highestPricePlayer, (PurchasableTile)_auctionTile, _highestPrice);
        GameManager.Instance.EventFinished();
    }
}
