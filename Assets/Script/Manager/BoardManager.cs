﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BoardManager : MonoBehaviour
{

    //singleton
    public static BoardManager Instance { get; private set; }

    [SerializeField] BoardData _boardData;
    public List<Tile> Board { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Board = new List<Tile>();
            if (_boardData != null)
            {
                CreateBoard();
            }
            else
            {
                LoadBoard();
            }
        }
        else
        {
            Destroy(this);
        }
    }

    //using this at the moment, 
    void LoadBoard()
    {
        Board.AddRange(transform.Find("Tiles").GetComponentsInChildren<Tile>());
    }

    //might use this to create the board programmatically
    void CreateBoard()
    {
        //assuming the board is always a square
        GameObject rect = Resources.Load<GameObject>("Prefab/GamePlay/RectTile");
        GameObject square = Resources.Load<GameObject>("Prefab/GamePlay/SquareTile");
        MeshRenderer center = transform.Find("CentrePiece").Find("Cube").GetComponent<MeshRenderer>();
        float squareSize = square.transform.Find("Cube-Visual").GetComponent<MeshRenderer>().bounds.size.y / 2;
        float centerSize = center.bounds.size.x;
        //0.6f is the scale of the parent.. should clean it up later
        float rectX = rect.transform.Find("Cube-Visual").GetComponent<MeshRenderer>().bounds.size.x * 0.6f;

        for (int y = 0; y < 4; y++)
        {
            float xOffSet = 0;
            float yOffSet = 0;
            float rotation = 0;
            switch (y)
            {
                case 0:
                    xOffSet = centerSize / 2 + squareSize;
                    yOffSet = centerSize / 2 + squareSize;
                    rotation = 90;
                    break;
                case 1:
                    xOffSet = centerSize / 2 + squareSize;
                    yOffSet = -centerSize / 2 - squareSize;
                    rotation = 0;
                    break;
                case 2:
                    xOffSet = -centerSize / 2 - squareSize;
                    yOffSet = -centerSize / 2 - squareSize;
                    rotation = -90;
                    break;
                case 3:
                    xOffSet = -centerSize / 2 - squareSize;
                    yOffSet = centerSize / 2 + squareSize;
                    rotation = 180;
                    break;
            }
            for (int k = 0; k < _boardData.TileDatas.Count / 4; k++)
            {
                GameObject go = (y * (_boardData.TileDatas.Count / 4) + k) % (_boardData.TileDatas.Count / 4) == 0 ?
                    Instantiate(square, transform.Find("Tiles")) : Instantiate(rect, transform.Find("Tiles"));
                float xPos = 0;
                float yPos = 0;
                switch (y)
                {
                    case 0:
                        xPos = xOffSet;
                        yPos = (-rectX) * k + yOffSet + ((k > 0) ? -squareSize + rectX / 2: 0);
                        break;
                    case 1:
                        xPos = (-rectX) * k + xOffSet + ((k > 0) ? -squareSize + rectX / 2 : 0);
                        yPos = yOffSet;
                        break;
                    case 2:
                        xPos = xOffSet;
                        yPos = (rectX) * k + yOffSet + ((k > 0) ? squareSize - rectX / 2 : 0);
                        break;
                    case 3:
                        xPos = (rectX) * k + xOffSet + ((k > 0) ? squareSize - rectX / 2 : 0);
                        yPos = yOffSet;
                        break;
                }
                go.transform.localPosition = new Vector3(xPos, yPos, 0);
                go.transform.localEulerAngles = new Vector3(0, 0, rotation);
                TileData tileData = _boardData.TileDatas[(y * _boardData.TileDatas.Count / 4) + k];
                Tile tile = tileData.AttachToGameObject(go);
                tile.SetTileData(tileData);
                Board.Add(tile);
            }
        }
    }
}