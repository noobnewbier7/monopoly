﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Yet again, another singleton
/// </summary>
public class GameplayCanvasManager : MonoBehaviour {

    public static GameplayCanvasManager Instance { get; private set; }

    [SerializeField] Canvas _tradePromptCanvas;
    [SerializeField] Canvas _tradeProcessCanvas;
    [SerializeField] Canvas _auctionCanvas;
    [SerializeField] Canvas _buyPropertyCanvas;
    [SerializeField] Canvas _defaultCanvas;
    [SerializeField] Canvas _rollDiceCanvas;
    [SerializeField] Canvas _manageCanvas;
    [SerializeField] Canvas _manageBrown;
    [SerializeField] Canvas _manageLightBlue;
    [SerializeField] Canvas _managePurple;
    [SerializeField] Canvas _manageOrange;
    [SerializeField] Canvas _manageRed;
    [SerializeField] Canvas _manageYellow;
    [SerializeField] Canvas _manageGreen;
    [SerializeField] Canvas _manageDarkBlue;
    [SerializeField] Canvas _manageBrownProp1;
    [SerializeField] Canvas _manageBrownProp2;
    [SerializeField] Canvas _manageLightBlueProp1;
    [SerializeField] Canvas _manageLightBlueProp2;
    [SerializeField] Canvas _manageLightBlueProp3;
    [SerializeField] Canvas _managePurpleProp1;
    [SerializeField] Canvas _managePurpleProp2;
    [SerializeField] Canvas _managePurpleProp3;
    [SerializeField] Canvas _manageOrangeProp1;
    [SerializeField] Canvas _manageOrangeProp2;
    [SerializeField] Canvas _manageOrangeProp3;
    [SerializeField] Canvas _manageRedProp1;
    [SerializeField] Canvas _manageRedProp2;
    [SerializeField] Canvas _manageRedProp3;
    [SerializeField] Canvas _manageYellowProp1;
    [SerializeField] Canvas _manageYellowProp2;
    [SerializeField] Canvas _manageYellowProp3;
    [SerializeField] Canvas _manageGreenProp1;
    [SerializeField] Canvas _manageGreenProp2;
    [SerializeField] Canvas _manageGreenProp3;
    [SerializeField] Canvas _manageDarkBlueProp1;
    [SerializeField] Canvas _manageDarkBlueProp2;



    //for trading process
    public Dictionary<GameObject, IPurchasable> PlayerATileReference { get; private set; }
    public Dictionary<GameObject, IPurchasable> PlayerBTileReference { get; private set; }

    //prefab
    [SerializeField] GameObject _propertyCard;

    List<Canvas> _canvases;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
        _canvases = new List<Canvas> { _tradeProcessCanvas,
            _tradePromptCanvas,
            _auctionCanvas,
            _buyPropertyCanvas,
            _defaultCanvas,
            _rollDiceCanvas,
            _manageCanvas,
            _manageBrown,
            _manageLightBlue,
            _managePurple,
            _manageOrange,
            _manageRed,
            _manageYellow,
            _manageGreen,
            _manageDarkBlue,
            _manageBrownProp1,
            _manageBrownProp2,
            _manageLightBlueProp1,
            _manageLightBlueProp2,
            _manageLightBlueProp3,
            _managePurpleProp1,
            _managePurpleProp2,
            _managePurpleProp3,
            _manageOrangeProp1,
            _manageOrangeProp2,
            _manageOrangeProp3,
            _manageRedProp1,
            _manageRedProp2,
            _manageRedProp3,
            _manageYellowProp1,
            _manageYellowProp2,
            _manageYellowProp3,
            _manageGreenProp1,
            _manageGreenProp2,
            _manageGreenProp3,
            _manageDarkBlueProp1,
            _manageDarkBlueProp2};
                            
    }

    /// <summary>
    /// use to turn on one canvas, it will also automatically turn off all the others
    /// </summary>
    /// <param name="canvas"></param>
    void TurnOnCanvas(Canvas canvas)
    {
        if (!_canvases.Contains(canvas))
            throw new ArgumentException("This canvas does not belong to the canvas manager");

        foreach (Canvas c in _canvases)
        {
            if(c == canvas)
            {
                c.gameObject.SetActive(true);
            }
            else
            {
                c.gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// use to display a canvas on top of another in case u need to
    /// </summary>
    /// <param name="canvas"></param>
    void TurnOnCanvasKeep(Canvas canvas)
    {
        if (!_canvases.Contains(canvas))
            throw new ArgumentException("This canvas does not belong to the canvas manager");
        canvas.gameObject.SetActive(true);
    }

    public void RefreshBuyPrompt(Sprite sprite, String propName, int price)
    {
        TurnOnCanvas(_buyPropertyCanvas);
        Image image = _buyPropertyCanvas.transform.Find("PreviewCard").GetComponent<Image>();
        image.sprite = sprite;

        Text labelText = _buyPropertyCanvas.transform.Find("Prompt").Find("PlayerLabel").GetComponent<Text>();
        labelText.text = GameManager.Instance.CurrentPlayer.Piece.name;

        Text promptRext = _buyPropertyCanvas.transform.Find("Prompt").Find("PromptText").GetComponent<Text>();
        promptRext.text = "Do you noob wanna buy " + propName + " for $" + price;
    }

    public void RefreshTrade()
    {
        TurnOnCanvas(_tradePromptCanvas);
        //deactivating everything first
        foreach (Transform child in _tradePromptCanvas.transform.Find("Cards"))
        {
            child.gameObject.SetActive(false);
        }

        foreach (Player player in GameManager.Instance.Players)
        {
            Transform card = _tradePromptCanvas.transform.Find("Cards").GetChild(player.PlayerSetting.PlayerNo);
            card.gameObject.SetActive(true);
            card.Find("Money").GetComponent<Text>().text = player.Money.ToString();
        }
    }

    public void ShowAuction()
    {
        TurnOnCanvas(_auctionCanvas);
        Image previewCard = _auctionCanvas.transform.Find("PreviewCard").GetComponent<Image>();
        previewCard.sprite = AuctionManager.Instance.AuctionTile.TileData.Sprite;
        foreach (Transform child in _auctionCanvas.transform.Find("Players"))
        {
            child.gameObject.SetActive(false);
        }
        foreach (Player player in AuctionManager.Instance.PlayerInAuction)
        {
            Transform card = _auctionCanvas.transform.Find("Players").GetChild(player.PlayerSetting.PlayerNo);
            card.gameObject.SetActive(true);
            card.Find("Money").GetComponent<Text>().text = player.Money.ToString();
        }
    }

    public void ShowRollDice()
    {
        TurnOnCanvas(_rollDiceCanvas);
    }

    public void ShowManager()
    {
        TurnOnCanvas(_manageCanvas);
    }

    public void ShowDefault()
    {
        TurnOnCanvas(_defaultCanvas);
    }

    public void ShowBrown()
    {
        TurnOnCanvas(_manageBrown);
    }

    public void ShowLightBlue()
    {
        TurnOnCanvas(_manageLightBlue);
    }

    public void ShowPurple()
    {
        TurnOnCanvas(_managePurple);
    }
    public void ShowOrange()
    {
        TurnOnCanvas(_manageOrange);
    }
    public void ShowRed()
    {
        TurnOnCanvas(_manageRed);
    }
    public void ShowYellow()
    {
        TurnOnCanvas(_manageYellow);
    }
    public void ShowGreen()
    {
        TurnOnCanvas(_manageGreen);
    }
    public void ShowDarkBlue()
    {
        TurnOnCanvas(_manageDarkBlue);
    }
    public void ShowBrownProp1()
    {
        TurnOnCanvasKeep(_manageBrownProp1);
    }
    public void ShowBrownProp2()
    {
        TurnOnCanvasKeep(_manageBrownProp2);
    }
    public void ShowLightBlueProp1()
    {
        TurnOnCanvasKeep(_manageLightBlueProp1);
    }
    public void ShowLightBlueProp2()
    {
        TurnOnCanvasKeep(_manageLightBlueProp2);
    }
    public void ShowLightBlueProp3()
    {
        TurnOnCanvasKeep(_manageLightBlueProp3);
    }
    public void ShowPurpleProp1()
    {
        TurnOnCanvasKeep(_managePurpleProp1);

    }
    public void ShowPurpleProp2()
    {
        TurnOnCanvasKeep(_managePurpleProp2);

    }
    public void ShowPurpleProp3()
    {
        TurnOnCanvasKeep(_managePurpleProp3);

    }
    public void ShowOrangeProp1()
    {
        TurnOnCanvasKeep(_manageOrangeProp1);
    }
    public void ShowOrangeProp2()
    {
        TurnOnCanvasKeep(_manageOrangeProp2);
    }
    public void ShowOrangeProp3()
    {
        TurnOnCanvasKeep(_manageOrangeProp3);
    }
    public void ShowRedProp1()
    {
        TurnOnCanvasKeep(_manageRedProp1);
    }
    public void ShowRedProp2()
    {
        TurnOnCanvasKeep(_manageRedProp2);
    }
    public void ShowRedProp3()
    {
        TurnOnCanvasKeep(_manageRedProp3);
    }
    public void ShowYellowProp1()
    {
        TurnOnCanvasKeep(_manageYellowProp1);
    }
    public void ShowYellowProp2()
    {
        TurnOnCanvasKeep(_manageYellowProp2);
    }
    public void ShowYellowProp3()
    {
        TurnOnCanvasKeep(_manageYellowProp3);
    }
    public void ShowGreenProp1()
    {
        TurnOnCanvasKeep(_manageGreenProp1);
    }
    public void ShowGreenProp2()
    {
        TurnOnCanvasKeep(_manageGreenProp1);
    }
    public void ShowGreenProp3()
    {
        TurnOnCanvasKeep(_manageGreenProp1);
    }
    public void ShowDarkBlueProp1()
    {
        TurnOnCanvasKeep(_manageDarkBlueProp1);
    }
    public void ShowDarkBlueProp2()
    {
        TurnOnCanvasKeep(_manageDarkBlueProp2);
    }





    public void RefreshDefault()
    {
        //TODO, the gold 
        TurnOnCanvas(_defaultCanvas);
        //deactivating everything first
        foreach (Transform child in _defaultCanvas.transform.Find("Players"))
        {
            child.gameObject.SetActive(false);
        }
        foreach (Player player in GameManager.Instance.Players)
        {
            Transform card = _defaultCanvas.transform.Find("Players").GetChild(player.PlayerSetting.PlayerNo);
            card.gameObject.SetActive(true);
            card.Find("Money").GetComponent<Text>().text = player.Money.ToString();
        }
    }

    public void RefreshTradeProcess()
    {
        TurnOnCanvas(_tradeProcessCanvas);
        PlayerATileReference = new Dictionary<GameObject, IPurchasable>(); // clear it every single fking time
        PlayerBTileReference = new Dictionary<GameObject, IPurchasable>(); // clear it every single fking time

        Transform horzLayout = _tradeProcessCanvas.transform.Find("VertLayout").Find("HorzLayout");
        //playerA props
        Transform playerAProps = horzLayout.Find("PlayerA").Find("Properties");
        foreach (IPurchasable purchasable in TradeManager.Instance.PlayerA.Properties)
        {
            //lets us assume everyone of them is a tile(ignore jail card, can be refactor later)
            GameObject card = Instantiate(_propertyCard, playerAProps);
            card.GetComponent<Image>().sprite = ((PurchasableTile)purchasable).TileData.Sprite;
            PlayerATileReference[card] = purchasable;
        }

        //playerB props
        Transform playerBProps = horzLayout.Find("PlayerA").Find("Properties");
        foreach (IPurchasable purchasable in TradeManager.Instance.PlayerB.Properties)
        {
            //lets us assume everyone of them is a tile(ignore jail card, can be refactor later)
            GameObject card = Instantiate(_propertyCard, playerBProps);
            card.GetComponent<Image>().sprite = ((PurchasableTile)purchasable).TileData.Sprite;
            PlayerBTileReference[card] = purchasable;
        }
    }

    public void DeactivateAll()
    {
        _canvases.ForEach(c => c.gameObject.SetActive(false));
    }
}
