﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/// <summary>
/// yet another singleton
/// </summary>
public class JailManager : MonoBehaviour {

    public static JailManager Instance { get; private set; }

    private const int BRIBE_AMOUNT = 50;
    private const int JAIL_TURN = 3;
    private Dictionary<Player, int> _jailStatus; // int indicates the turn remaining in jail, 0 means it is not in jail
    private Jail _jail;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            _jailStatus = new Dictionary<Player, int>();
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        _jail = (Jail)BoardManager.Instance.Board.Find(t => t.GetType() == typeof(Jail));
        GameManager.Instance.Players.ForEach(p => _jailStatus[p] = 0); // no player are in jail intially
    }

    public void SendToJail(Player player)
    {
        _jailStatus[player] = JAIL_TURN;
        GameManager.Instance.Teleport(player, _jail);
    }

    public bool InJail(Player player)
    {
        return _jailStatus[player] != 0;
    }

    public void Bribe(Player player)
    {
        if (!InJail(player))
        {
            throw new ArgumentException("player is not in jail");
        }
        GameManager.Instance.JailBribe();
        Bank.Instance.ReceiveFines(player, BRIBE_AMOUNT);
        Release(player);
    }

    public void UseCard(Player player)
    {
        if (!InJail(player))
        {
            throw new ArgumentException("player is not in jail");
        }
        //need to remove that noob card from player
        GameManager.Instance.JailBribe();
        Release(player);
    }

    /// <summary>
    /// I think sth wrong with these methods, it does not handle visiting... should it be a separate tile or wat not?
    /// </summary>
    public void Release(Player player)
    {
        _jailStatus[player] = 0;
        player.Piece.Goto(_jail); // just visiting
    }

    public void NewTurn(Player player)
    {
        _jailStatus[player] -= 1;
    }
}
