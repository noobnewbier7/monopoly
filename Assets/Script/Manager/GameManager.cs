﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;

public class GameManager : MonoBehaviour
{

    //singleton design
    public static GameManager Instance { get; private set; }
    public bool IsSettingUp { get; private set; }
    public List<Player> Players { get; private set; }
    public int TurnCount { get; set; }
    public int Dice { get; private set; }
    public Player CurrentPlayer { get { return _player; } private set { _player = value; } }
    public bool IsDouble { get; private set; } // is the dice a double?
    //returning all player finished their first lap
    public IEnumerable<Player> PlayersFinishedFirstLap { get { return _isFirstLab.Select(kvPair => kvPair.Key).TakeWhile(k => _isFirstLab[k]); } }
    public int GoBonus { get { return _goBonus; } }

    private Animator _statemachine;
    private Player _player;
    private Dictionary<Player, bool> _isFirstLab = new Dictionary<Player, bool>();
    private GameObject _piecePrefab;
    [SerializeField] private Dice _firstDice;
    [SerializeField] private Dice _secondDice;
    [SerializeField] private float _MoveAnimSpeed;
    [SerializeField] private int _initialMoney = 1500;
    [SerializeField] private int _goBonus = 200;


    private void Awake()
    {
        if (Instance == null)
        {
            IsSettingUp = true;
            _statemachine = GetComponent<Animator>();
            Players = new List<Player>();
            _piecePrefab = Resources.Load<GameObject>("Prefab/GamePlay/PlayerWrapper");
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        IsSettingUp = false;
        Debug.Log("starting");
        //temp
        foreach (PlayerSetting setting in GameOptionHolder.PlayerSettings)
        {
            Player player = InstantiatePlayer(setting);
            Players.Add(player);
            Debug.Log(Bank.Instance == null);
            Bank.Instance.GiveOutMoney(player, _initialMoney);
            player.Piece.Goto(BoardManager.Instance.Board.First());
        }
        Debug.Log(JailManager.Instance == null);
        _statemachine.SetTrigger("Ready");

        //for testing
        Time.timeScale = 3;
    }

    public void EndTurn()
    {
        //if there is no more properties you lost
        if (CurrentPlayer.Properties.Count == 0 && CurrentPlayer.Money < 0)
        {
            Debug.Log("Player dead");
            Players.Remove(CurrentPlayer);
            Destroy(CurrentPlayer.Piece.gameObject);
            //if only one player left, he wins
            if (Players.Count == 1)
            {
                _statemachine.SetTrigger("GameFinished");
            }
        }
        else
        {
            _statemachine.SetTrigger("PlayerFinished");
        }
    }

    public void SaveGame()
    {
        throw new NotImplementedException("Safe not implemented yet");
    }
    // Rememmber to change this when done with testing <=== oh my god Jamie you comment god

    //made it a wrapper so I can use coroutines
    public void RollDice()
    {
        _statemachine.SetTrigger("ThrowDice");
        StartCoroutine(RollDiceCoroutine());
    }

    private IEnumerator RollDiceCoroutine()
    {
        int firstDiceValue = 0;
        int secondDiceValue = 0;


        _firstDice.ResetPlace();
        _secondDice.ResetPlace();
        Coroutine firstDiceThrow = StartCoroutine(_firstDice.Throw(value => firstDiceValue = value));
        Coroutine secondDiceThrow = StartCoroutine(_secondDice.Throw(value => secondDiceValue = value));

        //wait till both coroutines are done
        yield return firstDiceThrow;
        yield return secondDiceThrow;

        //tobe deleted
        firstDiceValue = 40;

        IsDouble = firstDiceValue == secondDiceValue;
        Dice = firstDiceValue + secondDiceValue;
        _statemachine.SetTrigger("DiceSet");
        yield break;
    }


    #region Movement
    /*
     ALL movement only relates to the current player
         */

    //not passing anything
    public void Teleport(Player player, Tile destination)
    {
        player.Piece.Goto(destination);
    }

    //an override version, used for various purpose 
    public void MovePiece(Player player, TileData destination, Direction direction) // my instinct say I fked up the math
    {
        int destinationIndex = BoardManager.Instance.Board.FindIndex(t => t.TileData == destination);
        int currentIndex = BoardManager.Instance.Board.FindIndex(t => t == player.StandOn);
        if (destinationIndex == 0) // for easy manipulation
        {
            destinationIndex = BoardManager.Instance.Board.Count;
        }
        int step = 0; //stop compiler from complaining
        switch (direction)
        {
            case Direction.Forward:
                step = destinationIndex - currentIndex;
                if (step <= 0)
                {
                    step += BoardManager.Instance.Board.Count;
                }
                break;
            case Direction.BackWard:
                step = currentIndex - destinationIndex;
                if (step <= 0)
                {
                    step += BoardManager.Instance.Board.Count;
                }
                step = -step;
                break;
        }
        MovePiece(player ,step);
    }

    public void MovePiece(Player player, int step)
    {
        StartCoroutine(PieceAnim(player ,step));
    }

    // a wrapper method, as statemachine behaviour do not have access to StartCoroutine
    public void MovePiece(Player player)
    {
        StartCoroutine(PieceAnim(player ,Dice));
    }

    private IEnumerator PieceAnim(Player player, int steps)
    {
        //CameraControler.Instance.State = CameraControler.CameraState.StaticAndRotate;
        int index = BoardManager.Instance.Board.FindIndex(t => player.StandOn == t);
        // if it pass go
        if (index + steps > 40)
        {
            _isFirstLab[player] = true;
        }
        for (int i = 0; i != steps; i += (steps > 0) ? 1 : -1) // adapt for negative steps
        {
            float timer = 0;
            yield return new WaitUntil(() => (timer += Time.deltaTime) > _MoveAnimSpeed);
            index = ++index == BoardManager.Instance.Board.Count ? 0 : index; //reset if necessary
            player.Piece.Goto(BoardManager.Instance.Board[index]);
        }
        //switch of emmision of the last piece
        player.StandOn.Emmision(false);
        _statemachine.SetTrigger("FinishedMoving");
        yield break;
    }
    #endregion
    public void NextPlayer()
    {
        try
        {
            CurrentPlayer = Players[Players.FindIndex(p => p == CurrentPlayer) + 1];
}
        catch (ArgumentOutOfRangeException)
        {
            TurnCount++;
            CurrentPlayer = Players[0];
        }
        if (JailManager.Instance.InJail(CurrentPlayer))
        {
            _statemachine.SetTrigger("PlayerInJail");
        }
        else
        {
            _statemachine.SetTrigger("PlayerFree");
        }
    }

    #region Setup
    private Player InstantiatePlayer(PlayerSetting playerSetting)
    {
        GameObject gameObject = Instantiate(_piecePrefab, transform.Find("Players"));
        Instantiate(playerSetting.Model, gameObject.transform);
        gameObject.name = playerSetting.Model.name;

        Piece piece = gameObject.GetComponent<Piece>();
        Player player = new Player(playerSetting);
        piece.Represent = player;
        player.Piece = piece;

        //assign an agent to it if it is an AI
        if(playerSetting.PlayerType == PlayerType.AI)
        {
            AIAgent agent = gameObject.AddComponent<AIAgent>();
            agent.Player = player;
        }

        return player;
    }
    #endregion

    #region Misc and Temps
    public void MoveFor(int steps)
    {
        Dice = steps;
        MovePiece(CurrentPlayer);
    }

    #region wrapper for UI
    public void StartAuction()
    {
        AuctionManager.Instance.StartAuction((PurchasableTile)CurrentPlayer.StandOn);
    }

    public void Buy()
    {
        Debug.Log("Buying");
        Bank.Instance.Purchase(CurrentPlayer, (PurchasableTile)CurrentPlayer.StandOn);
        GameplayCanvasManager.Instance.DeactivateAll();
        EventFinished();
    }
    #endregion
    #endregion

    #region StateSetting
    public void EventFinished()
    {
        _statemachine.SetTrigger("EventFinished");
    }

    public void AllDecisionMade()
    {
        _statemachine.SetTrigger("DecisionMade");
    }

    public void JailBribe()
    {
        _statemachine.SetTrigger("Bribe");
    }
    #endregion
}
