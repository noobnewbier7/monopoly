﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum CardType
{
    Opportunity,
    PotLuck
}

/// <summary>
/// For opportunity and daldadladala card like that
/// </summary>
public class CardManager : MonoBehaviour
{
    [SerializeField] private DeckData _potLuck;
    [SerializeField] private DeckData _opportunities;

    public static CardManager Instance { get; private set; }

    private IEnumerator<CardData> _potLuckPile;
    private IEnumerator<CardData> _opportunityPile;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            _potLuck.ShuffleDeck();
            _opportunities.ShuffleDeck();
            _potLuckPile = _potLuck.CardDatas.GetEnumerator();
            _opportunityPile = _opportunities.CardDatas.GetEnumerator();
        }
        else
        {
            Destroy(this);
        }
    }

    /// <summary>
    /// Well, you draw a card, throw an exception if the card type is not implemented yet
    /// </summary>
    /// <param name="cardType"></param>
    /// <returns></returns>
    public Card DrawACard(CardType cardType)
    {
        Card card;
        switch (cardType)
        {
            case CardType.Opportunity:
                if (!_opportunityPile.MoveNext())
                {
                    _opportunityPile.Reset();
                }
                card = _opportunityPile.Current.CreateCard();
                break;
            case CardType.PotLuck:
                if (_potLuckPile.MoveNext())
                {
                    _potLuckPile.Reset();
                }
                card = _potLuckPile.Current.CreateCard();
                break;
            default:
                throw new ArgumentException("this card type is not implemented");
        }
        return card;
    }
}
