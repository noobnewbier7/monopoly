﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Yet another singleton
/// </summary>
public class SetUpCanvasManager : MonoBehaviour
{

    public static SetUpCanvasManager Instance { get; private set; }


    public bool _isAllowTrade { get; set; }
    public bool IsAbrige { get; set; }
    public int AbridgeTime { get; set; }
    private List<PlayerSettingGUI> _playerSettings = new List<PlayerSettingGUI>();

    [SerializeField] Text _playerNumText;
    [SerializeField] GameObject _playerSlot;
    [SerializeField] Transform _playerSlotsPanel;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        //adding the initial playerSettings
        _playerSettings.AddRange(GetComponentsInChildren<PlayerSettingGUI>());

    }

    public void AddOnePlayer()
    {
        int numOfPlayer = Int32.Parse(_playerNumText.text);
        if (numOfPlayer < 6)
        {
            numOfPlayer++;
            _playerSettings.Add(Instantiate(_playerSlot, _playerSlotsPanel).GetComponent<PlayerSettingGUI>());
        }
        _playerNumText.text = numOfPlayer.ToString();
    }

    public void MinusOnePlayer()
    {
        int numOfPlayer = Int32.Parse(_playerNumText.text);
        if (numOfPlayer > 1)
        {
            numOfPlayer--;
            //delete last child of the panel
            PlayerSettingGUI toRemove = _playerSettings[_playerSettings.Count - 1];
            _playerSettings.Remove(toRemove);
            Destroy(toRemove.gameObject);

        }
        _playerNumText.text = numOfPlayer.ToString();
    }

    /// <summary>
    /// return whether the game can be start or not
    /// </summary>
    /// <returns></returns>
    public bool SetInfo()
    {
        List<PlayerSetting> settings = _playerSettings.Select(gui => gui.GetInfo()).ToList();
        //find if there are any 2 player with the same model
        if (settings.Any(setting => settings.Any(other => other.Model == setting.Model && other != setting)))
        {
            return false;
        }
        GameOptionHolder.AbridgeTurn = AbridgeTime;
        GameOptionHolder.PlayerSettings = settings;
        GameOptionHolder.IsAllowTrade = _isAllowTrade;
        return true;
    }
}
