﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSetting
{
    public int PlayerNo { get; private set; }
    public PlayerType PlayerType { get; private set; }
    public Sprite PlayerIcon { get; private set; }
    public GameObject Model { get; private set; }
    //Human Player has difficulty of -1
    public int AIDifficulty { get; private set; }

    public PlayerSetting(int playerNo , PlayerType playerType, Sprite icon, GameObject model, int difficulty)
    {
        PlayerNo = playerNo;
        PlayerType = playerType;
        PlayerIcon = icon;
        Model = model;
        AIDifficulty = difficulty;
    }
}
