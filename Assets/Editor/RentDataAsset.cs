﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RentDataAsset
{
    [MenuItem("Assets/Create/RentData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<Rent>();
    }
}