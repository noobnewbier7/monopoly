﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BoardDataAsset
{
    [MenuItem("Assets/Create/BoardData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<BoardData>();
    }
}