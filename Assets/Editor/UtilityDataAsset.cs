﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class UtilityDataAsset
{

    [MenuItem("Assets/Create/UtilityData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<UtilityData>();
    }
}
