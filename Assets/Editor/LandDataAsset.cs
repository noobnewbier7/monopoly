﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LandDataAsset{
    
    [MenuItem("Assets/Create/LandData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<LandData>();
    }
}
