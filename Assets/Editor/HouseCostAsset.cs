﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class HouseCostAsset{

	[MenuItem("Assets/Create/HouseCostData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<HouseCost>();
    }
}
