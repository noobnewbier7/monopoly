﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SubmitOfferButton))]
public class SubmitOfferButtonEditor : Editor{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    }
}
