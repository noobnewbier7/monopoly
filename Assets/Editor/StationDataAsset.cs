﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class StationDataAsset{

    [MenuItem("Assets/Create/StationData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<StationData>();
    }
}
