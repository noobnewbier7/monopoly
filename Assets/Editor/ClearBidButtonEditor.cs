﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ClearBidButton))]
public class ClearBidButtonEditor : Editor{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    }
}
