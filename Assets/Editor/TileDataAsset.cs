﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TileDataAsset
{
    [MenuItem("Assets/Create/TileData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<TileData>();
    }
}
