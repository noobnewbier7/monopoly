﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class FreeParkingDataAsset{
    [MenuItem("Assets/Create/FreeParkingData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<FreeParkingData>();
    }
}
