﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GoDataAsset{

    [MenuItem("Assets/Create/GoData")]
	public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<GoData>();
    }
}
