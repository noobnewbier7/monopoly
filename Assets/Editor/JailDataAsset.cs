﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class JailDataAsset
{
    [MenuItem("Assets/Create/JailData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<JailData>();
    }
}
